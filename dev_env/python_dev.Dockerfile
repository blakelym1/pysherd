# repo config

# defined for dockerfile parsing
ARG REPO_SOURCE
ARG PYTHON_VERSION

# stage to get files from repo
FROM alpine/git AS builder_remote
ARG REPO_URI
ONBUILD RUN mkdir -p /root/.ssh && ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
ONBUILD RUN echo -e "# GitLab.com\nHost gitlab.com\n\tPreferredAuthentications publickey\n\tIdentityFile /root/.ssh/private_git_lab_key" > /root/.ssh/config
ONBUILD RUN --mount=type=secret,id=private_git_lab_key,target=/root/.ssh/private_git_lab_key git clone "git@$REPO_URI" /source

# stage to get local files
FROM alpine AS builder_local
ONBUILD COPY . /source

# choose which method to get files
# REPO_SOURCE should match the end of builder_${REPO_SOURCE}
FROM builder_$REPO_SOURCE as builder

# stage to download poetry
FROM python:$PYTHON_VERSION AS poetry_download
ARG POETRY_HOME
ARG POETRY_VERSION
RUN curl -sSL https://install.python-poetry.org | python3 -

# stage to download vs code
FROM python:$PYTHON_VERSION AS vscode_download
COPY dev_env/setup /tmp/setup
RUN chmod +x /tmp/setup/get_vs_code_server.sh
RUN /tmp/setup/get_vs_code_server.sh


FROM python:$PYTHON_VERSION
ARG POETRY_HOME
ARG POETRY_VERSION
ARG GIT_USER
ARG GIT_EMAIL

#configure image
#   user config
ENV USERNAME=develop \
    USER_UID=1000 \
    USER_GID=1000

#setup user
ENV SHELL=/bin/bash \
    H="/home/$USERNAME"

# install and update
RUN apt-get update && apt-get install -y \
    build-essential \
    curl \
    python-dev \
    git \
    sudo \
    openssh-client \
    tini \
 && rm -rf /var/lib/apt/lists/*

# get pip and poetry
RUN python -m ensurepip --upgrade
# RUN curl -sSL https://install.python-poetry.org | python3 -

# setup init scripts
# expand $POETRY_HOME into script, don't expand others
RUN echo "export POETRY_HOME=$POETRY_HOME" > /etc/profile.d/init_poetry.sh \
    && echo "export POETRY_VERSION=$POETRY_VERSION" >> /etc/profile.d/init_poetry.sh \
    && echo 'export PATH=$POETRY_HOME/bin:$PATH' >> /etc/profile.d/init_poetry.sh \
    && echo "export SHELL=$SHELL" > /etc/profile.d/default_shell.sh

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    #  Add sudo support.
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

COPY dev_env/setup/setup_ssh.sh  /etc/profile.d/

# Set the default user
USER $USER_UID
RUN git config --global user.name "$GIT_USER"
RUN git config --global user.email "$GIT_EMAIL"
COPY --chown=$USER_UID --from=poetry_download $POETRY_HOME $POETRY_HOME
COPY --chown=$USER_UID --from=vscode_download /root/.vscode-server $H/.vscode-server
# if user extensions are needed copy
#COPY --chown=$USER_UID --from=vscode_download /tmp/.vscode $H/.vscode

# we will have source and local files
# if the user wants to use files from the host machine, they will be mounted
# into /home/develop/local
# if the user wants to use downloaded files
# logic will move the files into local


# copy dependencies first so that file changes don't require a reinstall
COPY --chown=$USER_UID --from=builder /source/pyproject.toml $H/local/pyproject.toml
RUN $POETRY_HOME/bin/poetry -C $H/local install --no-root

# copy repo in so that it can be installed in venv
# remove the contents for conditional local or source logic
COPY --chown=$USER_UID --from=builder /source $H/local
RUN $POETRY_HOME/bin/poetry -C $H/local install --only-root
RUN sudo rm -rf $H/local && mkdir -p $H/local

# get source files
COPY --chown=$USER_UID --from=builder /source $H/source

# setup user preferences
COPY dev_env/setup/.bashrc $H
RUN mkdir $H/source/.vscode && find /home/develop/.cache/pypoetry/virtualenvs/ -maxdepth 1 -mindepth 1 | awk '{print "{\"python.defaultInterpreterPath\": \""$0"/bin/python\"}"}' > $H/source/.vscode/settings.json

# tini manages the process to pass signals through
# sleep infinity to make the container run until told to stop
ENTRYPOINT ["tini", "-v", "--", "bash", "-c", "sleep infinity"]